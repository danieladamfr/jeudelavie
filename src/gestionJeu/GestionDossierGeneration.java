package gestionJeu;

import liste.ListeChainee;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import generateur.*;

/**
 * La classe GestionDossierGeneration permet d'extraire d'un dossier un ensemble de fichier .LIF qui les transforme en
 * objet pour les ajouter a un ensemble lesFichier. De cet ensemble il est possible d'en extraire une page HTML
 * detaillant les informations d'un ou des fichierGeneration(s)
 *
 * @author Groupe 3
 * @see FichierGeneration
 */
public class GestionDossierGeneration {


    /**
     * @param pattern
     *        motif permettant d'utiliser les expressions rationnelles pour interroger des chaines de caracteres
     */
    private static Pattern pattern;
    /**
     * @param matcher
     *        représente le moteur de recherche d'un motif nomme pattern dans une chaine
     */
    private static Matcher matcher;
    /**
     * @param lesFichiers
     *        est un ensemble d'objet de type FichierGeneration
     */
    private ListeChainee<FichierGeneration> lesFichiers = new ListeChainee<FichierGeneration>();
    /**
     * @param limite
     *        est un entier qui fixe la fin de l'evolution de la generation intiale
     */
    private int limite;

    /**
     * @param generateur
     *        genere une nouvelle generation
     */
    private Generateur generateur;

    /**
     * @param dossier
     *        est un String qui porte le nom du repertoire dans lequel les fichiers .LIF se situent.
     */
    private String dossier;


    /**
     * Le constructeur GestionDossierGeneration est un generateur borne par une limite
     *
     * @param limite
     *        est un entier qui fixe la fin de l'evolution de la generation intiale
     * @param g
     *        genere une nouvelle generation
     */
    public GestionDossierGeneration(int limite, Generateur g) {
        this.limite = limite;
        generateur = g;
    }

    /**
     * La methode chargerDossier regarde si les fichiers verifient l'extention .LIF et cree des objets FichierGeneration
     * pour les ajouter ensuite à l'ensemble lesFichiers
     *
     * @param dossier
     *        est le nom du repertoire où se trouve les fichiers .LIF
     */
    public void chargerDossier(String dossier) {

        File[] files = null;
        File repertoireLifep = new File(dossier);
        files = repertoireLifep.listFiles();
        if(!repertoireLifep.exists()) {
            System.out.println("Le dossier n'existe pas");
            System.exit(1);
        }
        this.dossier = dossier+"/index.html";
        pattern = Pattern.compile("[a-zA-Z0-9]+.LIF$");
        for (int i = 0; i < files.length; i++) {
            //System.out.println(files[i].toString());
            matcher = pattern.matcher(files[i].toString());
            if (matcher.find()) {
                try {
                    lesFichiers.ajouter(new FichierGeneration(files[i].getPath(), generateur.generer(), limite));
                }catch(FileNotFoundException e){
                    System.out.println(e);
                }catch (Exception e){
                    System.out.println(e);
                }
            }

        }

    }

    /**
     * La methode chargerDossierHTML produit les resultats au format HTML sous forme de tableau et informe sur le type
     * de comportements d’un ensemble de jeux contenu par l'ensemble lesFichiers
     */
    public void chargerDossierHTML() throws IOException {
        String css =
                "<style> body, html{\n" +
                        "  height: 100%;\n" +
                        "  width: 100%;\n" +
                        "  padding: 0;\n" +
                        "  margin: 0;\n" +
                        "}\n" +
                        "table{\n" +
                        "  margin: auto;\n" +
                        "  border-collapse: collapse;\n" +
                        "  text-align: center;\n" +
                        "}\n" +
                        "tr:hover{\n" +
                        "  font-weight: bold;\n" +
                        "}\n" +
                        "th{\n" +
                        "  color: rgb(1,32,135);\n" +
                        "  border-bottom: 3px solid rgb(85,98,162);\n" +
                        "  width: 300px; \n" +
                        "} td{ color: rgb(83, 80, 135); padding-top: 5px;border-bottom: 1.5px solid rgb(193,193,193);}</style>";
        String head = "<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<meta charset=\"utf-8\">\n" + css + "<head>\n" + "<title>JEU DE LA VIE</title>\n" + "</head>" + "\n" + "<body> \n<table>" + "\n";
        String body0 = "			<tr> \n 				<th>Nom du fichier (.LIF)</th> \n 				<th>Type d'évolution</th> \n 				<th>Taille de la queue</th> \n 				<th>Taille de la boucle</th> \n	 		</tr>";
        String body1 = head + body0;
        String body2 = "";
        String body3 = "	\n		</table>" + "\n" + "</body>" + "\n" + "</html>";
        for (FichierGeneration f : lesFichiers) {
            body2 = ecrireHTML(f.getNom_fichier(), body2);
        }
        String html = body1 + body2 + body3;

        try {
            exporterVersHtml(html);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * La methode ecrireHTML insere une ligne pour chaque FichierGeneration dans le tableau de la methode
     * chargerDossierHTML informant sur le type de comportements d’un ensemble de jeux contenu
     *
     * @param path est le chemin du fichier .LIF contenu dans un FichierGeneration
     * @param html comporte l'ensemble des informations du type de comportements d'un ensemble de jeux contenu. Cette
     *             variable est utilisee pour l'insertion des informations d'un FichierGeneration par des lignes
     * @return retourne la variable html contenant l'ensemble des informations d'un FichierGenerations dans une ligne
     */
    public String ecrireHTML(String path, String html) throws IOException {
        File fichier = new File(path);
        FichierGeneration f = null;
        try {
            f = new FichierGeneration(path, generateur.generer(), this.limite);
        }catch(Exception e){
            return html;
        }
        int [] tab = f.getGeneration_initiale().typeEvolution(limite);


        return html += "			\n 			<tr> \n 				<td class=\"filename\">" + fichier.getName() + "</td> \n 				<td>" + getType(tab[0]) + "</td> \n 				<td>" + tab[1] + "</td> \n 				<td>" + getTailleBoucle(tab[2]) + "</td> \n	 		</tr>";
    }

    /**
     * La methode exporterVersHtml exporte le string HTML dans un fichier ayant pour extention .HTML dans le repertoire courant
     *
     * @param HTML
     *        cette variable contient l'ensemble des informations relatives aux FichierGeneration
     */
    public void exporterVersHtml(String HTML) throws Exception {
        try{

            BufferedWriter bw = new BufferedWriter(new FileWriter(dossier));
            PrintWriter pWriter = new PrintWriter(bw);
            pWriter.write(HTML);
            pWriter.close();
            System.out.println("Votre fichier index.html a été ajouté à l'adresse: "+dossier);
        } catch (Exception e) {
            throw new Exception("Probleme avec le repertoire de sorti: "+dossier);
        }

    }

    /**
     * La methode getType retourne le comportement des generations succedant la generation initiale.
     *
     * @param tab
     *        est un entier determinant le comportement des generations succedant la generation initiale.
     * @return String
     *         retourne la traduction du comportement des generations succedant la generation initiale sinon None.
     */
    public static String getType(int tab) {

        switch (tab)
        {
            case 0:
                return "Inconnu";
            case 1:
                return "Mort";
            case 2:
                return "Stable";
            case 3:
                return "Oscillateur";
            case 4:
                return "Vaisseau";
            default:
                return "None";
        }
    }


    /**
     * La methode getTailleBoucle retourne la taille de la boucle des generations succedant la generation initiale.
     * @param tab est un entier determinant la taille de la boucle des generations succedant la generation initiale.
     * @return String
     *         retourne la taille de la boucle des generations succedant la generation initiale sinon None.
     */
    public String getTailleBoucle(int tab) {
        switch (tab) {
            case -1:
                return "None";
            default:
                return String.valueOf(tab);

        }
    }


}
