package generation;

/**
 * Les cellules sont des objets comparable. Elles sont caracterisees des trois attributs qui les compose : son abcisse, son ordonnée, et son nombre de voisins.
 * Dans un graphique, l'abcisse correspond au numero de la colone d'un tableau, l'ordonnee correspond au numero de la ligne et enfin le nombre de voisin est l'entier correspondant au nombre de cellules existantes aux alentours de cette cellule.
 * Ici, les coordonnees sont positives ou nulles et le nombre de voisins correspond au nombre de cellules existantes directement adjacente à celle-ci.
 * 
 * @author Groupe 3
 */
public class Cellule implements Comparable<Cellule>{
    /** Champs de la classe cellules. */
    private int x;
    private int y;
    private int nb_voisins;

    /**
     * Construit une nouvelle cellule ayant pour abcisse le parametre "x" et pour ordonnee le parametre "y" et initiallise le nombre de voisins à 1.
     * @param x la valeur correspondant a l'abcisse de la nouvelle cellule
     * @param y la valeur correspondant a l'ordonne de la nouvelle cellule
     */
    public Cellule(int x, int y){
        this(x, y, 1);
    }

    /**
     * Construit une nouvelle cellule ayant pour abcisse le parametre "x", pour ordonnee le parametre "y" et initiallise le nombre de voisins à la valeur du parametre "nb_voisins" .
     * @param x
     *        la valeur correspondant a l'abcisse de la nouvelle cellule
     * @param y
     *        la valeur correspondant a l'ordonne de la nouvelle cellule
     * @param nb_voisins
     *        la valeur correspondant au nombre de voisins
     */
    public Cellule(int x, int y, int nb_voisins){
        this.x = x;
        this.y = y;
        this.nb_voisins = nb_voisins;
    }

    /**
     * Permet de modifier le nombre de voisins de l'element.
     * @param nb
     *        la valeure specifiee du nombre de voisins a mettre a jour
     */
    public void setNbVoisins(int nb){
        nb_voisins = nb;
    }

    /**
     * Retourne le nombre actuel de voisins de la cellules.
     * @return le nombre actuel de voisins de la cellules.
     */
    public int getVoisins(){
        return nb_voisins;
    }

    /**
     * Permet de modifier le nombre de voisins d'un element, en l'ajoutant au nombre de voisins du parametre.
     * Plus precisemment, la cellule appelante voit son nombre de voisins modifie, par la somme du nombre des voisins de la cellule appelante et du nombre des voisins de la cellule passee en parametre.
     *
     * @param c cellule dont le nombre de voisins sera additionne a celui de la cellule appelante.
     *
     */
    public void addNbVoisins(Cellule c){
        nb_voisins += c.nb_voisins;
    }

    /**
     * Copie un nouvelle cellule dont les abcisse et ordonnee sont modifié avec les parametres "x" et "y".
     * Plus concretement, clone une cellule puis decale son abcisse de "x" et son ordonnee de "y".
     *
     * @param x
     *        la valeur a ajouter, a l'abcisse existante, dans la nouvelle cellule.
     * @param y
     *        la valeur a ajouter, a l'ordonnee existante, dans la nouvelle cellule.
     * @return une nouvelle cellule ayant les coordonnees de "x" et "y" additionnees a celles de la cellule appelante.
     */
    public Cellule getCelluleModifiee(int x, int y){
        // retourne la cellule avec les coordonnées this.x+x et this.y+y
        return new Cellule(this.x+x, this.y+y);
    }

    /**
     * Retourne l'abcisse de la cellule.
     * @return l'abcisse de la cellule.
     */
    public int getX() { return x; }

    /**
     * Retourne l'ordonne de la cellule
     * @return l'ordonne de la cellule
     */
    public int getY() { return y; }

    /**
     * Retourne une representation de la cellule sous forme de chaîne de caractère.
     * La cellule est entourée de parentheses "()". les trois nombres correspondent respectivement a l'abcisees, l'ordonnee, et le nombre de voisins de la cellule.
     *
     * @return une representation de la cellule sous forme de chaîne de caractère.
     */
    public String toString(){ return "("+x+","+y+","+nb_voisins+")"; }

    /**
     * Compare cet objet avec la cellule passee en parametre. Retourne un entier negatif, zero ou un entier positif si la cellule est respectivement plus petit, egal ou plus grand que l'element specifie en parametre.
     * Les cellules sont comparees slon leurs coordonnees. Plus precisemment, elles sont d'abord comparees selon leur ordonnnee puis par leur abcisse.
     * @param c
     *        la cellule qui sera comparee a la cellule appelante
     * @return un entier negatif, positif ou nul en fonction de la comparaison des deux cellules entre elles.
     */
    @Override
    public int compareTo(Cellule c) {
        if( x == c.x && y == c.y )
            return 0;
        return (( y > c.y ) || ( y == c.y && x > c.x )) ? 1 : -1;
    }

    /**
     * Retourne true si les deux objets ont les memes caracteristiques.
     * Ici, true sera retourne si les deux objets sont tout deux des cellules ayant les memes coordonnees "x" et "y".
     *
     * @param o
     *        l'objet a compare avec la cellule appelante.
     * @return true si les deux objets ont les memes caracteristiques.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cellule cellule = (Cellule) o;
        return this.compareTo(cellule)==0;
    }
}
