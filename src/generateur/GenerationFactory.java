package generateur;


import generation.*;
import gestionJeu.JeuGeneration;

/**
 * La classe GenerationFactory est utilisé comme une usine à Generateur.
 * Un générateur sert plus concretement à générer un objet de type JeuGeneration.
 *
 * @see JeuGeneration
 * @see Generation
 * @see GenerationOuverte
 * @see GenerationBouclee
 * @see GenerationFermee
 * @see Generateur
 * @author Groupe 3
 */
public class GenerationFactory {

    /**
     *  Retourne un objet Generateur permettant de générer une nouvelle génération ouverte.
     *
     *  @see GenerationOuverte
     *
     *  @return Un générateur permettant de générer une Génération ouverte.
     */
    public static Generateur generateurGenerationOuverte(){
        return GenerationOuverte::new;
    }

    /**
     *  Retourne un objet Generateur permettant de générer une nouvelle génération bouclée.
     *
     *  @see GenerationBouclee
     *
     *  @return Un générateur permettant de générer une Génération Bouclée.
     */
    public static Generateur generateurGenerationBouclee(){
        return GenerationBouclee::new;
    }

    /**
     *  Retourne un objet Generateur permettant de générer une nouvelle génération fermée.
     *
     *  @see GenerationFermee
     *
     *  @return Un générateur permettant de générer une Génération Fermée.
     */
    public static Generateur generateurGenerationFermee(){
        return GenerationFermee::new;
    }
}
