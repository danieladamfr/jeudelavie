package gestionJeu;



import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * La classe FichierGeneration permet d'acceder aux informations d'un Fichier .LIF.
 *
 * @author Groupe 3
 */
public class FichierGeneration implements Comparable<FichierGeneration> {

    /**
     * @param generation_initiale correspond a la premiere generation. Cette ensemble comporte l'ensemble des cellules
     *                            pour un fichier .LIF donne.
     */
    private JeuGeneration generation_initiale;
    /**
     * @param comportement est une variable determinant dans quel univers se situe l'execution du jeu.
     */
    private String comportement;
    /**
     * @param nom_fichier correspond au nom du fichier d'un .LIF
     */
    private String nom_fichier;
    /**
     * @param limite est un entier qui borne l'univers dans lequel evolue la generation initiale. 
     *
     */
    private int limite;
    /**
     * @param pattern
     *        motif permettant d'utiliser les expressions rationnelles pour interroger des chaines de caracteres
     */
    private static Pattern pattern;
    /**
     * @param matcher
     *        représente le moteur de recherche d'un motif nomme pattern dans une chaine
     */
    private static Matcher matcher;

    /**
     * Le construteur FichierGeneration est defini par une generation initiale comportant l'ensemble des coordonnees
     * des cellules d'un fichier .LIF avec son nom et par une limite qui borne l'univers dans lequel evolue la
     * generation initiale.
     * @param nom_fichier est un string nommant le fichier .LIF
     * @param generation_initale est un JeuGeneration qui est un ensemble dans lequel l'ensemble des coordonnes des
     *                           cellules d'un fichier .LIF sont reunies.
     *
     * @param limite est un entier qui borne l'univers dans lequel evolue la generation initiale.
     */
    public FichierGeneration(String nom_fichier, JeuGeneration generation_initale, int limite) throws Exception{

        this.nom_fichier = nom_fichier;
        this.generation_initiale = generation_initale;
        this.limite = limite;
        chargerFichier(nom_fichier);
            //comportement = generation_initiale.definirComportement(limite);
    }

    /**
     * La methode getNom_fichier retourne le d'un fichier .LIF
     * @return nom_fichier est un string correspondant au nom d'un fichier .LIF
     */
    public String getNom_fichier() {
        return nom_fichier;
    }

    /**
     * La methode getLimite retourne un entier qui borne l'univers dans lequel evolue la generation initiale.
     * @return limite est entier qui borne l'univers dans lequel evolue la generation initiale.
     */
    public int getLimite() {
        return limite;
    }

    /**
     * La methode getGeneration_initiale retourne un JeuGeneration, c'est le jeu extrait a partir des coordonnees d'un fichier .LIF.
     * @return generation_initiale qui est issue des informations d'un fichier .LIF.
     */
    public JeuGeneration getGeneration_initiale() {
        return generation_initiale;
    }



    /**
     * La methode comportement retourne l'univers dans lequel la generation initiale evolue
     * @return comportement est un string correspondant a l'univers dans lequel la generation initiale evolue
     */
    public String getComportement() {
        return comportement;
    }

    /**
     * La methode lireFichierLif prend un fichier en parametre ayant pour extention .LIF.
     * Elle itere sur chaque ligne du fichier jusqu'a trouver une ligne avec "#P x y" les coordonnees d'initialisation.
     * Quand ces coordonnees sont trouves alors l'iteration se poursuit jusqu'aux lignes contenant "." ou "*".
     * Si une "*" est detectee alors ses coordonnees sont utilisees pour instancier une cellule pour etre ensuite
     * ajoutee a un ensemble de generations. L'iteration se poursuit jusqu'a ce que le fichier contienne une ligne vide.
     * @param path cette variable correspond au chemin fichier ayant pour extention .LIF.
     * @throws Exception une exception est levee si pour un fichier .LIF donne celui-ci comporte plus de 22 commentaires.
     */
    public void chargerFichier(String path) throws Exception {

        File f = new File(path);

        String Elements2[];
        Elements2 = path.split("/");


        //if(!Elements2[Elements2.length-1].matches("[[:print:]]+.LIF$")) {
        if(!(f.exists() && f.getName().matches("[a-zA-Z0-9/\\-]+.LIF$"))) {

            System.out.println("Le fichier : "+f.getName()+" n'est pas un fichier .LIF ou n'existe pas");
            throw new FileNotFoundException();

        } else {
            String line = "";
            BufferedReader br1 = new BufferedReader(new FileReader(path));
            BufferedReader br2 = new BufferedReader(new FileReader(path));
            String[] Elements;
            // Coordonnees d'initialisation
            int initialisation_px = 0;
            int initialisation_py = 0;
            // Coordonnees de la cellule
            int px = 0;
            int py = 0;
            // Coordonnees tampons
            int index_px = 0;
            int index_py = 0;
            // Test commentaires
            int compteurCommentaire = 0;
            while ((line = br1.readLine()) != null || compteurCommentaire > 22) {
                if (line.matches("^#D.*")){
                    compteurCommentaire++;
                }
            }
            br1.close();
            if (compteurCommentaire <= 22) {
                while ((line = br2.readLine()) != null) {
                    if(line.length() > 80) {
                        System.out.println("Une des lignes du fichier comporte plus de 80 caracteres");
                        throw new Exception("Une des lignes du fichier comporte plus de 80 caracteres");
                    }
                    if (line.matches("^#P.*")) {
                        Elements = line.split(" ");
                        initialisation_px = Integer.parseInt(Elements[1]);
                        initialisation_py = Integer.parseInt(Elements[2]);
                        //System.out.println("#P : ( " + initialisation_px + " ; " + initialisation_py + " )");
                        index_px = 0;
                        index_py = 0;
                    } else {
                        for (int i = 0; i < line.length(); i++) {
                            if (line.charAt(i) == '*') {
                                px = initialisation_px + index_px;
                                py = initialisation_py + index_py;
                                //System.out.println("Coordonnees d'une cellule : ( " + initialisation_px + " ; " + initialisation_py + " )");
                                generation_initiale.ajouterCellule(px, py);
                            }
                            index_px++;
                        }
                        index_px = 0;
                        index_py++;
                    }
                }
            } else {

                System.out.println("Le fichier comporte plus de 22 commentaires");
                throw new Exception("#D > 22");
            }
            br2.close();
        }
    }


    /**
     * La methode simulation affiche le resultat d'un jeuGeneration, l'utilisateur perçoit les generations successivement.
     * Pour que l'affichage soit dynamique, l'affichage du Terminal est "clear" a chaque generations en utilisant la ligne de code
     * ci-contre "\033[H\033[2J"
     * 'H' se déplacer sur la partie supérieure de la console
     * '2J' nettoyer la console
     * \033 code ESCAPE de ANSI 
     */
       public void simulation(){
        JeuGeneration g = generation_initiale;
        int i = 1;
        System.out.println("Début de la simulation");
        while(i <= limite && !g.estVide()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("\033[H\033[2J");
            System.out.flush();
            System.out.println(g);
            g = g.nextGeneration();
            i++;
        }
    }

    /**
     * La methode compareTo compare les noms des fichier .LIF pour les ordonner dans l'ordre lexicographique.
     *
     * @param o correspond a un FichierGeneration donne
     * @return un entier determinant le classement lexicographique
     */
    @Override
    public int compareTo(FichierGeneration o) {
        return nom_fichier.compareTo(o.nom_fichier);
    }

    /**
     * La methode toString retourne une vue du nom du fichier choisie avec son comportement pour un FichierGeneration
     *
     * @return String une chaine de caractere composee du nom du fichier et du comportement  d'un FichierGeneration
     */
    public String toString(){
        return "Fichier: "+nom_fichier+"\t -> "+comportement;
    }
}
