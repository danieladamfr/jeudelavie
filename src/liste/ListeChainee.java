package liste;

import java.util.Comparator;
import java.util.Iterator;

/**
 * Liste chaînée et triée par une relation d'ordre totale, ce qui correspond à un ensemble. Elle est de type générique et est itérable.
 *
 * @param <T> Le type utilisé dans l'implémentation de cette liste chainée.
 * 
 * @author Groupe 3
 */
public class ListeChainee<T> implements Iterable<T> {
    private Maillon<Comparable<T>> tete;
    private Comparator<Comparable> comparateur = (x, y)->x.compareTo(y);

    /**
     * Construit un nouvel ensemble vide, trié selon l'ordre naturel de cet élément.
     */
    public ListeChainee(){
        tete=null;
        comparateur=(x,y)->(x.compareTo(y));
    }

    /**
     * Construit un nouvel ensemble vide, trié selon l'ordre naturel de cet élément. Ce constructeur permet d'eablir sa propre comparaison des elements.
     * @param c
     *        le comparateur utilisé pour trier les elements de ce nouvelle ensemble.
     */
    public ListeChainee(Comparator<Comparable> c){
        comparateur = c;
    }

    /**
     * Retourne le nombre d'éléments dans cette Liste.
     *
     * @return le nombre d'éléments dans cette Liste.
     */
    public int taille(){
        Maillon m = getTete();
        int taille = 0;
        for (T ignored : this){
            taille++;
        }
        return taille;
    }

    /**
     * Cette méthode est à manipuler avec d'extrêmes précautions. C'est un ajout très particulier et dangereux pour une Liste Chainée triée, car elle ajoute l'élément directement en tête de la Liste Chainée.
     * Cet ajout doit être utilisée si et uniquement si la tête de la Liste (accessible grace à la méthode getTête()) est supérieur à l'élément à ajouter.
     * @param element l'élément à ajouter dans la Liste
     */
    public void ajouterPremier(Comparable<T> element){

        tete = new Maillon(element,tete);
    }

    /**
     * Ajoute l'élément spécifié à cette Liste si l'élément n'est pas encore présent.
     * Plus précisemment, ajoute l'élément "element" si et seulement si cette Liste ne contient pas d'element "e2" tel que (element==null ? e2==null : element.equals(e2)).
     * @param element l'élément à ajouter dans la Liste
     * @return true si la Liste ne contient pas encore l'élément
     */
    public boolean ajouter(Comparable<T> element) {
        //Pas d'ajout si pas d'element ou element déjà présent
        if (element == null || contient(element)) return false;

        Maillon<Comparable<T>> maillEle = new Maillon(element);
        //Ajouter dans un liste vide
        if (this.estVide()) {
            tete = maillEle;
            return true;
        }
        Maillon<Comparable<T>> pointeur = getTete();
        //Ajouter en tete
        if (comparateur.compare((pointeur.valeur), (maillEle.valeur)) > 0) {
            maillEle.suivant = pointeur;
            tete = maillEle;
            return true;
        }
        //Ajout dans la liste
        while (pointeur.suivant != null) {
            if (comparateur.compare(pointeur.suivant.valeur, maillEle.valeur) == 1) {
                maillEle.suivant = pointeur.suivant;
                pointeur.suivant = maillEle;
                return true;
            }
            pointeur = pointeur.suivant;
        }
        //Ajouter en queue
        pointeur.suivant = maillEle;
        return true;
    }

    /**
     * Supprime l'élément spécifié de cette Liste si l'élément y est présent.
     * Plus précisemment, supprime l'élément "element" si et seulement si cette Liste contient un élément "e2" tel que (element==null ? e2==null : element.equals(e2)).
     * @param element l'élément à supprimer de la Liste
     * @return true si la Liste ne contient pas encore l'élément
     */
    public boolean supprimer(Comparable<T> element){
        if (!contient(element)) return false;
        if (tete.valeur.equals(element)){
            tete=tete.suivant;
            return true;
        }
        Maillon pointeur=getTete();
        do {
            if (pointeur.suivant.valeur.equals(element)){
                pointeur.suivant=pointeur.suivant.suivant;
                return true;
            }
            pointeur=pointeur.suivant;
        }while (pointeur != null);
        return false;
    }

    /**
     * Retourne true si l'ensemble ne contient aucun élément.
     * @return true si l'ensemble ne contient aucun élément.
     */
    public boolean estVide(){

        return getTete()==null;
    }

    /**
     * Retourne true si l'ensemble contient l'élément spécifié "element".
     * Plus précisemment, retourne true si l'élément "element" existe dans l'ensenble, avec "e2" tel que (element==null ? e2==null : element.equals(e2)).
     * @param element élément dont l'existence dans l'ensemble est vérifié.
     * @return true si si l'ensemble contient l'élément spécifié "element".
     */
    public boolean contient(Comparable<T> element) {

        Maillon m = getTete();
        while(m != null) {
            if (m.valeur.equals(element)) {
                return true;
            }else {
                m = m.suivant;
            }
        }
        return false;

    }

    /**
     * Supprime tout les éléments de l'ensemble. Il sera alors vide.
     */
    public void nettoyer(){

        tete=null;
    }

    /**
     * Methode créant un itérateur de l'objet appelant.
     *
     * @return Un itérateur capable de parcourir l'ensemble.
     * @see LC_Iterator
     */
    @Override
    public Iterator<T> iterator() {

        return new LC_Iterator(getTete());
    }

    /**
     * Retourne une representation de l'ensemble sous forme de chaîne de caractère.
     * L'ensemble est entouré de crochets "[]" et les éléments de cet ensemble sont séparés par des virgules ", ".
     *
     * @return une representation de l'ensemble sous forme de chaîne de caractère.
     */
    @Override
    public String toString() {
        String res ="{";
        int lol = 0;
        if (!estVide()) {
            for (T m : this) {
                lol++;
                res += m.toString()+"  ";
            }
        }
        return res + "}";
    }

    /**
     * Retourne la tête le la liste, soit l'élément le plus petit de cet ensemble.
     *
     * @return le maillon correspondant à la tête de la liste.
     * @see Maillon
     */
    public Maillon getTete() {

        return tete;
    }

    /**
     * Retourne {@code true} si l'ensemble appelant contient exactement les memes elements que l'ensemble specifie.
     * @param o
     *        objet qui sera compare a l'ensemble appelant.
     * @return true si les deux objets sont des ensembles contenant exactement les memes elements, a l'element pres.
     */
    public boolean equals(Object o){
        if(o == null || !(o instanceof ListeChainee))
            return false;
        if(o == this)
            return true;
        ListeChainee<T> l = (ListeChainee<T>) o;
        Maillon l_liste = l.tete;
        Maillon this_liste = tete;
        while(l_liste != null && this_liste != null){
            try {
                if (comparateur.compare(l_liste.valeur, this_liste.valeur) == 0) {
                    l_liste = l_liste.suivant;
                    this_liste = this_liste.suivant;
                } else
                    return false;
            } catch (ClassCastException e){
                return false;
            }
        }
        return l_liste == null && this_liste == null;
    }
}
