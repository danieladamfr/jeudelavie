package gestionJeu;


/**
 *  L'inteface JeuGeneration est un outil permettant le deroulement du jeu
 *  Une fois implémentée sur une classe, cette dernière pourra être utilisée à travers le terminal.
 * 
 * @author Groupe 3
 */
public interface JeuGeneration {

    /**
     *  Calcule et retourne la génération suivante de la génération appelante.
     *
     *  @return Generation La génération suivante par rapport à this.
     */
    JeuGeneration nextGeneration();

    /**
     * La methode ajouterCellule permet d'ajouter une cellule a un ensemble de generation_initiale.
     *
     * @param x est un entier representant une coordonnee d'une cellule.
     * @param y est un entier representant une coordonnee d'une cellule
     * @see
     */
    void ajouterCellule(int x,int y);


    int[] typeEvolution(int t);


    /**
     *  Test si la génération est vide ou si elle contient des cellules.
     *
     *  @return boolean True si la génération ne contient plus de génération, False sinon.
     */
    boolean estVide();
}